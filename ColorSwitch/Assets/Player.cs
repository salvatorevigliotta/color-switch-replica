﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    public float JumpForce = 10f;

    //TODO Private
    public Rigidbody2D rb;
    public SpriteRenderer sr;

    public string CurrentColor;

    public Color colorCyan;
    public Color colorYellow;
    public Color colorMagenta;
    public Color colorPink;

	// Use this for initialization
	void Start ()
    {
        SetRandomColor();
		//TODO Inizialize Rigidbody
	}

    private void SetRandomColor()
    {
        int index = Random.Range(0, 4);

        switch(index)
        {
            case 0:
                CurrentColor = "Cyan";
                sr.color = colorCyan;
                break;
            case 1:
                CurrentColor = "Yellow";
                sr.color = colorYellow;
                break;
            case 2:
                CurrentColor = "Magenta";
                sr.color = colorMagenta;
                break;
            case 3:
                CurrentColor = "Pink";
                sr.color = colorPink;
                break;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if(Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
        {
            rb.velocity = Vector2.up * JumpForce;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "ColorChanger")
        {
            SetRandomColor();
            Destroy(collision.gameObject);
        }          
        else if(collision.tag != CurrentColor)
        {
            Debug.Log("GAME OVER !!!");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
